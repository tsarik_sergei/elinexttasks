package task1;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import pages.TutbyHomePage;
import pages.TutbySearchResultsPage;
import utils.WebDriverFactory;

import java.util.ArrayList;

public class TestTutBySearchWD {

    private WebDriver driver;
    private final static Logger logger = Logger.getLogger(TestTutBySearchWD.class);

    @BeforeMethod(description = "tut.by WD search test - start browser", groups = {"tutby"})
    public void beforeMethod() {
        driver = WebDriverFactory.getInstance().getDriver();
        driver.manage().window().maximize();
    }

    @Test(description = "tut.by WD search test", priority = 2, groups = {"tutby"})
    public void tutbySearchTestWithWD() {
        new TutbyHomePage(driver).open().fillSearchInputAndGo("Лукашенко");
        ArrayList<String> searchResultsLinks = new TutbySearchResultsPage(driver).getSearchResultsList();
        for (int i = 0; i < searchResultsLinks.size(); i++) {
            logger.info(searchResultsLinks.get(i));
        }
    }

    @AfterMethod(description = "tut.by WD search test - quit browser", groups = {"tutby"})
    public void afterMethod() {
        WebDriverFactory.getInstance().removeDriver();
    }
}