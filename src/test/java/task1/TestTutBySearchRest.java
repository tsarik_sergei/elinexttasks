package task1;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import utils.HTTPClient;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestTutBySearchRest {

    private final static Logger logger = Logger.getLogger(TestTutBySearchRest.class);

    @Test(description = "tut.by REST search test", priority = 1, groups = {"tutby"})
    public void tutbySearchTestRest() throws IOException {
        Pattern ptnLink = Pattern.compile("<a class=\"b-url__a\" href=\".*\" target=\"_blank\">");
        Pattern ptnSplit = Pattern.compile("\"");
        Matcher matcher = ptnLink.matcher(new HTTPClient().doGetRequest());
        while(matcher.find()) {
            logger.info(ptnSplit.split(matcher.group())[3]);
        }
    }
}