package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HiltonHomePage extends AbstractPage {

    private static final By ADVANCED_SEARCH_LINK = By.xpath("//span[contains(text(), 'ADVANCED SEARCH')]");
    private static final By ADDRESS_INPUT = By.xpath("//input[@id='hotelSearchOneBox']");
    private static final By ROOMS_INPUT = By.xpath("//select[@id='numberOfRooms']");
    private static final By ADULTS_INPUT = By.xpath("//select[@id='room1Adults']");
    private static final By CHILDREN_INPUT = By.xpath("//select[@id='room1Children']");
    private static final By CHECK_IN_INPUT = By.xpath("//input[@id='checkin']");
    private static final By CHECK_OUT_INPUT = By.xpath("//input[@id='checkout']");
    private static final By GO_BUTTON = By.xpath("//span[contains(text(), ' Go')]");

    private static final String URL_HOME_PAGE = "https://www3.hilton.com/en/index.html?ignoreGateway=true";

    public HiltonHomePage(WebDriver driver) {
        super(driver);
    }

    public HiltonSearchResultsPage fillSearchInputAndGo(String query, String checkIn, String checkOut, int adults, int children, int rooms) {
        waitForElementVisible(ADVANCED_SEARCH_LINK);
        driver.findElement(ADVANCED_SEARCH_LINK).click();
        waitForElementVisible(ADULTS_INPUT);
        driver.findElement(ADULTS_INPUT).click();
        selectFromDropDown(ADULTS_INPUT, adults);
        driver.findElement(CHILDREN_INPUT).click();
        selectFromDropDown(CHILDREN_INPUT, children);
        driver.findElement(ROOMS_INPUT).click();
        selectFromDropDown(ROOMS_INPUT, rooms);
        driver.findElement(ADDRESS_INPUT).sendKeys(query);
        driver.findElement(CHECK_IN_INPUT).click();
        driver.findElement(CHECK_IN_INPUT).clear();
        driver.findElement(CHECK_IN_INPUT).sendKeys(checkIn);
        driver.findElement(CHECK_OUT_INPUT).click();
        driver.findElement(CHECK_OUT_INPUT).clear();
        driver.findElement(CHECK_OUT_INPUT).sendKeys(checkOut);
        driver.findElement(GO_BUTTON).click();
        return new HiltonSearchResultsPage(driver);
    }

    public HiltonHomePage selectFromDropDown(By dropDownXpath, int value) {
        String strXpath = dropDownXpath.toString() + "/option[@value='" + value + "']";
        driver.findElement(By.xpath(strXpath.replace("By.xpath: ", ""))).click();
        return this;
    }

    public HiltonHomePage open() {
        driver.get(URL_HOME_PAGE);
        return this;
    }
}