package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Iterator;

public class TutbySearchResultsPage extends AbstractPage {

    private static final By RESULT_LINK_PATTERN_LOCATOR = By.xpath("//li[@class='b-results__li']/h3/a[1]");

    public TutbySearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public ArrayList<String> getSearchResultsList(){
        ArrayList<String> searchResultLinks = new ArrayList<>();
        Iterator<WebElement> i = driver.findElements(RESULT_LINK_PATTERN_LOCATOR).iterator();
        while (i.hasNext()) {
            searchResultLinks.add(i.next().getAttribute("href"));
        }
        return searchResultLinks;
    }
}