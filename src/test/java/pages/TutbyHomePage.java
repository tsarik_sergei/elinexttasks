package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TutbyHomePage extends AbstractPage {

    private static final By SEARCH_INPUT_LOCATOR = By.id("search_from_str");
    private static final By GO_BUTTON_LOCATOR = By.name("search");

    private static final String URL_HOME_PAGE = "https://www.tut.by/";

    public TutbyHomePage(WebDriver driver) {
        super(driver);
    }

    public TutbySearchResultsPage fillSearchInputAndGo(String query) {
        waitForElementVisible(SEARCH_INPUT_LOCATOR);
        driver.findElement(SEARCH_INPUT_LOCATOR).sendKeys(query);
        driver.findElement(GO_BUTTON_LOCATOR).click();
        return new TutbySearchResultsPage(driver);
    }

    public TutbyHomePage open() {
        driver.get(URL_HOME_PAGE);
        return this;
    }
}