package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HiltonSearchResultsPage extends AbstractPage {

    private String bookMeassage;

    private final static Logger logger = Logger.getLogger(HiltonSearchResultsPage.class);

    private static final By SELECT_HOTEL_BUTTON = By.xpath("//span[contains(text(), 'Select')]//ancestor::a");
    private static final By SELECT_HOTEL_DESCRIPTION = By.xpath("//span[contains(text(), 'Select')]//ancestor::div[@class='callout calloutCash']/following::div[@class='hotelDescription']/h2/a/span");
    private static final By FIRST_NAME_INPUT = By.id("guestFirstName");
    private static final By LAST_NAME_INPUT = By.id("guestLastName");
    private static final By PHONE_INPUT = By.id("guestPhone");
    private static final By EMAIL_NAME_INPUT = By.id("guestEmail");
    private static final By ADDRESS_NAME_INPUT = By.id("guestUSAddress1");
    private static final By ZIP_NAME_INPUT = By.id("guestUSZip");
    private static final By CITY_NAME_INPUT = By.id("guestUSCity");
    private static final By STATE_NAME_INPUT = By.id("guestUSState");
    private static final By CONTINUE_BUTTON = By.xpath("//div[@class='paraSubmitButton']/a[@class='linkBtn ']");
    private static final By TOTAL_FOR_STAY_AMOUNT = By.xpath("//span[contains(text(),'Total for stay')]//following-sibling::span");
    private static final By CHAT_NOW_BUTTON = By.id("tfs_invite_mvp_invite_action0");
    private static final By SORT_BY_DROP_DOWN = By.xpath("//select[@name='sort']");
    private static final By SORT_BY_PRICE_OPTION = By.xpath("//select[@name='sort']/option[@value='PRICE']");
    private static final By BOOK_OR_SELECT_ROOM_BUTTON = By.xpath("//div[@class='quickLookBox']//following-sibling::div[@class='callout calloutCash']//descendant::a");
    private static final By CAPTCHA_WARNING = By.xpath("//p[@id='235']");
    private static final By SEARCH_RESULTS_UL_SELECT_ROOM = By.xpath("//ul[@class='rate-wrapper']");
    private static final By SEARCH_RESULTS_LI_SELECT_ROOM = By.xpath("//ul[@class='rate-wrapper']/li/div[@class='priceamount-wrapper']/span[@class='priceamount currencyCode-USD strikethrough confidential']/ins | //ul[@class='rate-wrapper']/li/div[@class='priceamount-wrapper']/span[@class='priceamount currencyCode-USD']");
    private static final By SEARCH_RESULT_UL_QUICK_BOOK = By.xpath("//div[@role='main']/div/div[2]/div/ul");
    private static final By SEARCH_RESULT_LI_QUICK_BOOK = By.xpath("//div[@role='main']/div/div[2]/div/ul/li/div/div/button/div/div[2]");

    public HiltonSearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public HiltonSearchResultsPage clickSelectHotel() {
        scrollTo(SELECT_HOTEL_BUTTON);
        logger.info(driver.findElement(SELECT_HOTEL_DESCRIPTION).getText());
        driver.findElement(SELECT_HOTEL_BUTTON).click();
        return new HiltonSearchResultsPage(driver);
    }

    public HiltonSearchResultsPage fillFormGuestInformation() throws InterruptedException {
        waitForElementVisible(FIRST_NAME_INPUT);
        driver.findElement(FIRST_NAME_INPUT).sendKeys("John");
        driver.findElement(LAST_NAME_INPUT).sendKeys("Black");
        driver.findElement(PHONE_INPUT).sendKeys("212-201-3608");
        driver.findElement(EMAIL_NAME_INPUT).sendKeys("Black@mail.com");
        driver.findElement(ADDRESS_NAME_INPUT).sendKeys("6 Jackson Street");
        driver.findElement(ZIP_NAME_INPUT).sendKeys("10028");
        driver.findElement(CITY_NAME_INPUT).sendKeys("New York");
        driver.findElement(STATE_NAME_INPUT).sendKeys("New York");
        driver.findElement(CONTINUE_BUTTON).click();
        return new HiltonSearchResultsPage(driver);
    }

    public String getTotalForStay() {
        waitForElementVisible(TOTAL_FOR_STAY_AMOUNT);
        return driver.findElement(TOTAL_FOR_STAY_AMOUNT).getText();
    }

    public HiltonSearchResultsPage clickMaxPriceItemOnPage() {
        Collection<WebElement> searchResults;
        waitForElementVisible(SORT_BY_DROP_DOWN);
        driver.findElement(SORT_BY_DROP_DOWN).click();
        waitForElementVisible(SORT_BY_PRICE_OPTION);
        driver.findElement(SORT_BY_PRICE_OPTION).click();
        waitForElementVisible(BOOK_OR_SELECT_ROOM_BUTTON);
        searchResults = driver.findElements(BOOK_OR_SELECT_ROOM_BUTTON);
        String xpathItemMaxPrice = "//div[@id='hotelsEndlessScrolling']/fieldset[" +
                searchResults.size() +
                "]/div[@class='sResult clear']/div[@class='callout calloutCash']/div[@class='calloutBox ']//a";
        waitForElementPresent(By.xpath(xpathItemMaxPrice));
        driver.findElement(By.xpath(xpathItemMaxPrice)).click();
        return this;
    }

    public void bookRoom() throws InterruptedException {
        if (isTagContainsText("h2", "Step 2 of 5")) {
            clickMaxPriceSelectRoom();
        } else {
            clickMaxPriceQuickBook();
        }
    }

    public HiltonSearchResultsPage clickMaxPriceQuickBook() {
        waitForElementVisible(SEARCH_RESULT_UL_QUICK_BOOK);
        List<WebElement> searchResults = driver.findElements(SEARCH_RESULT_LI_QUICK_BOOK);
        driver.findElement(By.xpath("//div[@role='main']/div/div[2]/div/ul/li[" + getMaxPrice(searchResults) + "]/div/div/button[1]")).click();
        bookMeassage = "Total for stay " + getTotalForStay();
        return this;
    }

    public HiltonSearchResultsPage clickMaxPriceSelectRoom() throws InterruptedException {
        waitForElementVisible(SEARCH_RESULTS_UL_SELECT_ROOM);
        List<WebElement> searchResults = driver.findElements(SEARCH_RESULTS_LI_SELECT_ROOM);
        driver.findElement(By.xpath("//ul[@class='rate-wrapper']/li[" + getMaxPrice(searchResults) + "]/div[3]/form/a")).click();
        fillFormGuestInformation();
        waitForElementVisible(CAPTCHA_WARNING);
        bookMeassage = driver.findElement(CAPTCHA_WARNING).getText();
        return this;
    }

    public boolean isTagContainsText(String tag, String text) {
        waitForPageLoad();
        waitForElementPresent(By.tagName("title"));
        Pattern ptnTagWithText = Pattern.compile("<" + tag + ".*>" + text + "</" + tag + ">");
        Matcher matcher = ptnTagWithText.matcher(driver.getPageSource());
        return matcher.find();
    }

    private int getMaxPrice(List<WebElement> searchResults) {
        int iMaxPrice = 0;
        int maxPrice = 0;
        int currentPrice = 0;
        Pattern p = Pattern.compile("\\d+");
        Matcher m;
        for (int i = 0; i < searchResults.size(); i++) {
            m = p.matcher(searchResults.get(i).getText());
            while (m.find()) {
                currentPrice = Integer.parseInt(m.group());
            }
            if (currentPrice > maxPrice) {
                maxPrice = currentPrice;
                iMaxPrice = i + 1;
            }
        }
        logger.info("Price $" + maxPrice);
        return iMaxPrice;
    }

    public String getBookMessage(){
        return this.bookMeassage;
    }
}