package task3;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import pages.HiltonHomePage;
import pages.HiltonSearchResultsPage;
import utils.WebDriverFactory;
import utils.DateGenerator;

import java.util.concurrent.TimeUnit;

public class HiltonSearchWithWD {

    private final static Logger logger = Logger.getLogger(HiltonSearchWithWD.class);
    private WebDriver driver;

    @BeforeMethod(description = "hilton.com WD search test - start browser")
    public void beforeMethod() {
        driver = WebDriverFactory.getInstance().getDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test(description = "hilton.com WD search test", priority = 3)
    public void hiltonSearchTestWithWD() throws InterruptedException {
        HiltonSearchResultsPage webPage = new HiltonHomePage(driver).open().fillSearchInputAndGo("Washington, DC", DateGenerator.newDate(1), DateGenerator.newDate(8), 2, 0, 1);
        webPage.clickSelectHotel();
        webPage.bookRoom();
        logger.info(webPage.getBookMessage());
    }

    @AfterMethod(description = "hilton.com WD search test - quit browser")
    public void afterMethod() {
        WebDriverFactory.getInstance().removeDriver();
    }
}