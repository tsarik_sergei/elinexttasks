package task4;

import org.testng.annotations.*;
import utils.FTPClient;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

public class TestFTPCommands {

    private static final String FTP_HOST = "ftp.drivehq.com";
    private static final int FTP_PORT = 21;
    private static final String USER = "tsarik";
    private static final String PASSWORD = "drivehqTsar1k";
    private static final String TEST_DIRECTORY_NAME = "/testDirectory";

    private FTPClient ftpClient = new FTPClient();

    @BeforeClass(description = "FTP commands test - start FTP connection")
    public void beforeClass() throws IOException, InterruptedException {
        ftpClient.login(FTP_HOST, FTP_PORT, USER, PASSWORD);
    }

    @Test(description = "FTP commands test", priority = 1)
    public void ftpCommandsTest() throws IOException, InterruptedException {
        Collection<String> ftpDirectories = ftpClient.getDirectoryList("/");
        Iterator list = ftpDirectories.iterator();
        while (list.hasNext()) {
            ftpClient.changeWorkingDirectory(list.next().toString());
        }
        if (ftpClient.makeDirectory(TEST_DIRECTORY_NAME)){
            ftpClient.removeDirectory(TEST_DIRECTORY_NAME);
        }
        ftpClient.quitFTPconnection();
    }

    @AfterClass(description = "FTP commands test - quit FTP connection")
    public void afterClass() throws IOException, InterruptedException {
        ftpClient.quitFTPconnection();
    }
}