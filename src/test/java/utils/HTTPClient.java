package utils;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.log4j.Logger;
import java.io.IOException;

public class HTTPClient {

    private final static Logger logger = Logger.getLogger(HTTPClient.class);

    public String doGetRequest() throws IOException {
        OkHttpClient httpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://search.tut.by/?status=1&ru=1&encoding=1&page=0&how=rlv&query=%D0%9B%D1%83%D0%BA%D0%B0%D1%88%D0%B5%D0%BD%D0%BA%D0%BE")
                .get()
                .addHeader("Referer", "https://www.tut.by/")
                .addHeader("Cookie", "anyCookie")
                .build();
        Response response = null;
        try {
            response = httpClient.newCall(request).execute();
        } catch (IOException e) {
            logger.error(e);
        }
        return (response != null ? response.body() != null ? response.body().string() : null : "0");
    }
}