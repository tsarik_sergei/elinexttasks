package utils;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.*;

public class FTPClient {

    private BufferedReader reader, readerDirList;
    private OutputStreamWriter writer;
    private String serverOutputForPasvCommand;
    private String pasvHost;
    private int pasvPort;
    private Collection<String> ftpDirectories;
    private Socket socket = null;
    private String currentDirectory = "/";
    private String serverResponseLine = "";
    private Socket socketPassiveMode = null;

    private final static Logger logger = Logger.getLogger(FTPClient.class);

    public synchronized void makeConnection(String host, int port) throws IOException {
        if (socket != null) {
            throw new IOException("FTP is already connected. Disconnect first.");
        }
        socket = new Socket(host, port);
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new OutputStreamWriter(socket.getOutputStream());

        (new Thread(() -> {
            try {
                serverResponseLine = reader.readLine();
                while (serverResponseLine != null) {
                    if (serverResponseLine.startsWith("1") || serverResponseLine.startsWith("2") || serverResponseLine.startsWith("3")) {
                        logger.info("SERVER: " + serverResponseLine);
                        if (serverResponseLine.startsWith("227 Entering Passive Mode")) {
                            serverOutputForPasvCommand = serverResponseLine;
                        }
                    } else {
                        logger.error("SERVER: " + serverResponseLine);
                    }
                    serverResponseLine = reader.readLine();
                }
            } catch (IOException e) {
                logger.error(e);
            }
        })).start();
    }

    public synchronized void sendCommand(String command) throws IOException, InterruptedException {
        writer.write(command + "\r\n");
        writer.flush();
        logger.info("COMMAND: " + command);
        Thread.sleep(1000);
        if (command.equals("PASV")) {
            makePasvConnection();
            logger.info("Host : " + pasvHost);
            logger.info("Port : " + pasvPort);
            socketPassiveMode = new Socket(pasvHost, pasvPort);
            readerDirList = new BufferedReader(new InputStreamReader(socketPassiveMode.getInputStream()));
        } else if (command.equals("LIST")) {
            ftpDirectories = new LinkedList<>();
            String dir = readerDirList.readLine();
            while (dir != null) {
                if (dir.startsWith("d")) {
                    dir = "/" + dir.substring(52);
                    logger.info(dir);
                    ftpDirectories.add(dir);
                }
                dir = readerDirList.readLine();
            }
            if (ftpDirectories.isEmpty()) {
                logger.info("There are no subdirectories in " + currentDirectory);
            }
            socketPassiveMode.close();
        }
        Thread.sleep(1000);
    }

    public synchronized String getServerResponseString(){
        return serverResponseLine;
    }

    public synchronized Collection<String> getDirectoryList(String directory) throws IOException, InterruptedException {
        currentDirectory = directory;
        sendCommand("CWD " + currentDirectory);
        sendCommand("PASV");
        sendCommand("PWD");
        sendCommand("LIST");
        return this.ftpDirectories;
    }

    private synchronized void makePasvConnection() {
        String fullAddress = serverOutputForPasvCommand.substring(serverOutputForPasvCommand.indexOf("(") + 1, serverOutputForPasvCommand.indexOf(")"));
        List<String> partsOfAdress = Arrays.asList(fullAddress.replaceAll("\\s+", "").split(","));
        pasvHost = partsOfAdress.get(0) + "." + partsOfAdress.get(1) + "." + partsOfAdress.get(2) + "." + partsOfAdress.get(3);
        pasvPort = Integer.parseInt(partsOfAdress.get(4)) * 256 + Integer.parseInt(partsOfAdress.get(5));
    }

    public synchronized void login(String host, int port, String user, String pass) throws IOException, InterruptedException {
        makeConnection(host, port);
        sendCommand("USER " + user);
        sendCommand("PASS " + pass);
    }

    public synchronized boolean makeDirectory(String name) throws IOException, InterruptedException {
        sendCommand("MKD " + name);
        return getServerResponseString().startsWith("257 Directory created successfully") ? true : false;
    }

    public synchronized boolean removeDirectory(String name) throws IOException, InterruptedException {
        sendCommand("RMD " + name);
        return getServerResponseString().startsWith("250 Directory deleted successfully") ? true : false;
    }

    public synchronized void printWorkingDirectory() throws IOException, InterruptedException {
        sendCommand("PWD");
    }

    public synchronized boolean changeWorkingDirectory(String name) throws IOException, InterruptedException {
        sendCommand("CWD " + name);
        return getServerResponseString().startsWith("250 CWD command successful") ? true : false;

    }

    public synchronized void quitFTPconnection() throws IOException, InterruptedException {
        if (!socket.isClosed()) {
            sendCommand("QUIT");
        }
    }
}