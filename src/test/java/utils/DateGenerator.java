package utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateGenerator {

    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy MM dd");

    public static String newDate(int daysAfter){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, daysAfter);
        return SDF.format(cal.getTime());
    }
}